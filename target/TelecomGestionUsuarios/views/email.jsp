<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es"> 
<head> 
<title>Bienvenidos a Telecom</title>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
 <style type="text/css"> 
 *{ margin: 0; padding: 0; } 
 body { background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; font-size: 62.5%; } 
 a:link { color: #000000; font-weight: bold; } 
 a:active { color: #000000; font-weight: bold; } 
 a:visited { color: #000000; font-weight: bold; } 
 a:hover { color: #FF0000; font-weight: bold; } 
 .page-container { border: solid 1px rgb(150, 150, 150); font-size: 1.0em; margin: 0px auto; margin-bottom: 10px; margin-top: 10px; width: 700px; } 
 .header { font-family: "trebuchet ms", arial, sans-serif; height: 82px; overflow: hidden; overflow: visible !important; width: 700px; } 
 .main-content { display: inline; float: left; margin: 0 0 0 30px; margin-bottom: 10px !important; margin-bottom: 5px; overflow: hidden; overflow: visible !important; width: 640px; } 
 .main-content h1.pagetitle { color: #000000; font-family: Verdana, Geneva, sans-serif; font-size: 180%; font-weight: bold; margin: 0 0 0.4em 0; padding: 0 0 2px 0; text-align:center; } 
 .main-content p { color: #000000; font-size: 120%; line-height: 1.5em; margin: 0 0 1.0em 0; text-align: justify; } 
 .main-content table { background-color: rgb(233, 232, 244); border-collapse: collapse; clear: both; empty-cells: show; margin: 0 0 0 0; table-layout: fixed; text-align: justify; width: 100%; } 
 .main-content table td { background-color: rgb(225, 225, 225); border-bottom: solid 2px rgb(255, 255, 255); border-left: solid 2px rgb(255, 255, 255); border-right: solid 2px rgb(255, 255, 255); border-top: solid 2px rgb(255, 255, 255); color: #000000; font-size: 11.5px; font-weight: normal; height: 2.5em; padding: 2px 5px 2px 5px; text-align: center; } 
 .footer { background: rgb(225, 225, 225); clear: both; font-size: 1.0em; height: 7em; overflow: hidden; overflow: visible !important; padding: 1.1em 0 0; width: 700px; } 
 .footer p { color: rgb(125, 125, 125); font-size: 104%; font-weight: bold; line-height: 1.3em; text-align: center; } 
 </style> 
 </head>  
 <body> 
 <div class="page-container"> 
 	<div class="header"></div> <br> 
 	<div class="main-content"> 
 		<h1 class="pagetitle" style="COLOR: #e66817;">Verifica tu cuenta Telecom</h1> 
 		<p>Hola, <b>@NOMBRE.</b></p>
 		<p>Gracias por registrarte en Telecom.</p> 
 		<p>Para verificar tu cuenta y empezar a disfrutar de los beneficios de Telecom, por favor comprueba que esta es tu direcci&oacute;n de correo electr&oacute;nico haciendo clic en el siguiente enlace:</p> 
 		<p><a href="@URL">@URL</a></p>
 		<p>Si la URL anterior no funciona, intenta copiarla y pegarla en tu navegador de internet. Si continuas teniendo problemas, por favor cont&aacute;ctanos a soporte@addcel.com</p>
 		<p>Tu informaci&oacute;n estar&aacute; segura de acuerdo a nuestras pol&iacute;ticas de privacidad. Por favor visita nuestro sitio web <a href="www.mobilecard.mx">www.mobilecard.mx</a> para leer los t&eacute;rminos de uso.</p>
 		<p><font size=+1>¡Gracias por unirte a la comunidad Telecom!</font></p>
 		
 		 <h3>&nbsp;</h3>  </div>  <div class="footer"> <p>Nota. Este correo es de caracter informativo, no es necesario que responda al mismo.</p> <br />
 		 <p>Telecom - ABC Capital &copy; 2015 | Todos los derechos reservados.</p> <p>Powered by Addcel.</p> </div> </div> 
 </body> </html>