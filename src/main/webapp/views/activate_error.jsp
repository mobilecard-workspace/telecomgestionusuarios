<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<%@ page isELIgnored="false" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width" />
<link href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAC/0lEQVQ4T6XS+0vVdxzH8ef78z16cq4dSIaUTcpLF/xhYadyy6ytuTLyB81LjaK8IBVESyiYxCLapW3RyLXGzrpYCt0oTjWXTbLOmpY5o2Oji9HUI5pLtvJCl3M83/cg+w/2+uX12+Onp4TqK1X/akIcYOaswqQsQX27oK8NxIC7GJk8D/UfR1ur0OfDyJQsZH45EhmFhGq/1PDZT5BIsJLfxZScg13JYIchyoUUVKM9rdCwA6Jj0ecDMD0X/bsds7YOsf8NaHBrPFZMLBLsx6SthTtnEIcTXHGw+jzsnQFvpUH+EbTnBtrswW7yYMrqR4GRbfEYdz6m/xYM9yHuIui4BNExkP09HFkC8enw4U703i/Qdwv7yh4ku/IVsD0ea1YhMukduPQ5UnoRvCWjQMEJOJAO490w2Ac9f6CpJajvK2R1LTJycbfqz+WY8UlI3kFkqBec0dCwdfSzvoPA79B7AxIXw+NO9NkgdF+Dknpk5MIXSqcPE2FB5nYkbia0HYWADyIiIGU5xKXB7VNw/wK8eIqOS4b0TTDGhYQ8OUr7WayYiWAEyauC8xvB6YTEhdD7G8xYDykfwfUfwLcTtaJh+BEsqEDCbV61awqwVhyGXyuQwhq46wUBZhZD7RoorAfnWBjoBs9cmLsFfWMCOKKQsN+rdnU+1rJKuP4jsvBT6G4C0VHAuxJWXYZAI4ydCIezIHEROrsMjAMJHd+gNO/DuJcjHQ2QsRm5fRocDpi6FPyHICETOq/A9Dxo3INOykAftoG7FAm3+1Qv70ZedyFJ85E3p8HNarAckLoG/DWgNi9bT8lDW/bDSBB1upB5HyPBqiJloAsTCTI1E3ltHLR4kOAQbLgJJ1dCzBToboZZZWhjJSQswA60QuzbyItv3lcroxQzeTYQhobPYOghPPoTKaqDq3vhSRf6uAtWHIO6CpizDh3sx/afQUK+nzR8chMyJgJH7tdobTkmd9/LIkl8Dzp8kPQB/PMAJqRiN+8HVwL2k36snG8RVVX+x/4D734yc+k8tqYAAAAASUVORK5CYII=" rel="icon" type="image/x-icon" />
<title>TELECOM</title>
</head>
<body>
	<h3>${message}</h3>
</body>
</html>