<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es"> 
<head> 
<title>Welcome to Telecom</title>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
 <style type="text/css"> 
 *{ margin: 0; padding: 0; } 
 body { background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; font-size: 62.5%; } 
 a:link { color: #000000; font-weight: bold; } 
 a:active { color: #000000; font-weight: bold; } 
 a:visited { color: #000000; font-weight: bold; } 
 a:hover { color: #FF0000; font-weight: bold; } 
 .page-container { border: solid 1px rgb(150, 150, 150); font-size: 1.0em; margin: 0px auto; margin-bottom: 10px; margin-top: 10px; width: 700px; } 
 .header { font-family: "trebuchet ms", arial, sans-serif; height: 82px; overflow: hidden; overflow: visible !important; width: 700px; } 
 .main-content { display: inline; float: left; margin: 0 0 0 30px; margin-bottom: 10px !important; margin-bottom: 5px; overflow: hidden; overflow: visible !important; width: 640px; } 
 .main-content h1.pagetitle { color: #000000; font-family: Verdana, Geneva, sans-serif; font-size: 180%; font-weight: bold; margin: 0 0 0.4em 0; padding: 0 0 2px 0; text-align:center; } 
 .main-content p { color: #000000; font-size: 120%; line-height: 1.5em; margin: 0 0 1.0em 0; text-align: justify; } 
 .main-content table { background-color: rgb(233, 232, 244); border-collapse: collapse; clear: both; empty-cells: show; margin: 0 0 0 0; table-layout: fixed; text-align: justify; width: 100%; } 
 .main-content table td { background-color: rgb(225, 225, 225); border-bottom: solid 2px rgb(255, 255, 255); border-left: solid 2px rgb(255, 255, 255); border-right: solid 2px rgb(255, 255, 255); border-top: solid 2px rgb(255, 255, 255); color: #000000; font-size: 11.5px; font-weight: normal; height: 2.5em; padding: 2px 5px 2px 5px; text-align: center; } 
 .footer { background: rgb(225, 225, 225); clear: both; font-size: 1.0em; height: 7em; overflow: hidden; overflow: visible !important; padding: 1.1em 0 0; width: 700px; } 
 .footer p { color: rgb(125, 125, 125); font-size: 104%; font-weight: bold; line-height: 1.3em; text-align: center; } 
 </style> 
 </head>  
 <body> 
 <div class="page-container"> 
 	<div class="header"></div> <br> 
 	<div class="main-content"> 
 		<h1 class="pagetitle" style="COLOR: #e66817;">Verify your Telecom account</h1> 
 		<p>Hi, <b>@NOMBRE.</b></p>
 		<p>Thanks for registering with Telecom.</p> 
 		<p>To verify your account and start enjoying the Telecom benefits, please let us know that this is your e-mail address by clicking the link below:</p> 
 		<p><a href="@URL">@URL</a></p>
 		<p>If you have trouble accessing the link above, try copying and pasting the link into your web browser. If you continue having problems, please contact us soporte@addcel.com</p>
 		<p>Your information will be safe according to our privacy policies. Please visit our web site <a href="www.mobilecard.mx">www.mobilecard.mx</a> to read our terms of use.</p>
 		<p><font size=+1>¡Gracias por unirte a la comunidad Telecom!</font></p>
 		
 		 <h3>&nbsp;</h3>  </div>  <div class="footer"> <p>Note. This email is informative, you do not need to respond to it.</p> <br />
 		 <p>Telecom - ABC Capital &copy; 2015 | All rights reserved.</p> <p>Powered by Addcel.</p> </div> </div> 
 </body> </html>