package com.addcel.telecom.usuarios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.addcel.telecom.usuarios.services.PasswordService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class PasswordServiciosController {
	
	private static final String PATH_ACTUALIZA_PASSWORD = "/telecom/actualiza/password";
	
	private static final String PATH_RESET_PASSWORD = "/telecom/reset/password";
	
	private static final String PARAM_JSON = "json";
	
	@Autowired
	private PasswordService passwordService;
	
	@RequestMapping(value = PATH_ACTUALIZA_PASSWORD, method=RequestMethod.POST)
	public @ResponseBody String cambiarPassword(@RequestParam(PARAM_JSON) String json) {
		return passwordService.cambioPassword(json);
	}
	
	@RequestMapping(value = PATH_RESET_PASSWORD, method=RequestMethod.POST)
	public @ResponseBody String resetPassword(@RequestParam(PARAM_JSON) String json) {
		return passwordService.resetPassword(json);
	}
	
}