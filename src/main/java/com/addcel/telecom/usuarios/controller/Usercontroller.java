package com.addcel.telecom.usuarios.controller;

import javax.servlet.http.HttpServletRequest;

import org.bouncycastle.util.encoders.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.telecom.usuarios.model.vo.BaseResponse;
import com.addcel.telecom.usuarios.model.vo.InsertRequest;
import com.addcel.telecom.usuarios.model.vo.LoginRequest;
import com.addcel.telecom.usuarios.model.vo.TUsuarios;
import com.addcel.telecom.usuarios.model.vo.TermResponse;
import com.addcel.telecom.usuarios.model.vo.UpdateResponse;
import com.addcel.telecom.usuarios.model.vo.UsuarioVO;
import com.addcel.telecom.usuarios.model.vo.VerifyResponse;
import com.addcel.telecom.usuarios.services.UserServices;



@Controller
public class Usercontroller {

	
	@Autowired
	UserServices userServices;
	
	@RequestMapping(value = "{idioma}/user/insert", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<TUsuarios> UserInsert( @PathVariable String idioma, @RequestBody InsertRequest request) {
		TUsuarios user = userServices.UserInsert(request, idioma);
		return new ResponseEntity<TUsuarios>(user,HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "{idioma}/user/login", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<TUsuarios> login(@PathVariable String idioma, @RequestBody LoginRequest request){
		return new ResponseEntity<TUsuarios>(userServices.userLogin(request, idioma),HttpStatus.OK);
	}
	
	@RequestMapping(value = "{idioma}/user/verify", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<VerifyResponse> verify(@PathVariable String idioma, @RequestParam("idUsuario") long idUsuario){
		return new ResponseEntity<VerifyResponse>(userServices.statusVerify(idUsuario, idioma),HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "{idioma}/{idUsuario}/password/update", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public  ResponseEntity<BaseResponse> PasswordUpdate(@PathVariable String idioma, @PathVariable String idUsuario, @RequestParam("old") String pass_old,  @RequestParam("new") String pass_new){
		
		return new ResponseEntity<BaseResponse>(userServices.passwordUpdate(idioma, idUsuario, pass_old, pass_new),HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "{idioma}/password/reset", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<BaseResponse> PasswordReset(@PathVariable String idioma,@RequestParam("userOrEmail") String userOrEmail){
		
		return new ResponseEntity<BaseResponse>(userServices.passwordReset(userOrEmail, idioma),HttpStatus.OK);
		
	}
	
	/**ACTIVAR CUENTA DE USUARIO**/
	@RequestMapping(value = "{idioma}/user/activate", method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public ModelAndView Useractivate(@PathVariable String idioma, @RequestParam("codigo") String cadena){
		
		return userServices.userActivate(cadena, idioma);
	}
	
	/**ENVIAR CORREO PARA CONFIRMACION DE CUENTA**/
	@RequestMapping(value = "{idioma}/{idUsuario}/activate", method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public  ResponseEntity<BaseResponse> ActivationSendMail(@PathVariable String idioma,  @PathVariable long idUsuario){
		
		return new ResponseEntity<BaseResponse>(userServices.ActivationSendMail(idioma, idUsuario),HttpStatus.OK);
	}
	
	@RequestMapping(value = "{idioma}/{idUsuario}/{name}/update", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<UpdateResponse> UserUpdate(@PathVariable String idioma,@PathVariable String name, @PathVariable long idUsuario, @RequestParam("value") String value){
		return new ResponseEntity<UpdateResponse>(userServices.userUpdateInfo(idioma, name, idUsuario, value),HttpStatus.OK);
	}
	
	@RequestMapping(value = "terminos/{idpais}/{idioma}", method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public ResponseEntity<TermResponse> getTerminos(@PathVariable String idioma, @PathVariable int idpais, HttpServletRequest req){
		String pass = req.getHeader("client");
		
		return new ResponseEntity<TermResponse>(userServices.getTerms(idioma, idpais, pass),HttpStatus.OK);
	}
	
}
