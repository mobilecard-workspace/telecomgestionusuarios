package com.addcel.telecom.usuarios.controller;

import com.addcel.telecom.usuarios.model.vo.TUsuarios;
import com.addcel.telecom.usuarios.model.vo.UsuariosUsa;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.telecom.usuarios.services.UsuarioService;
import com.addcel.telecom.usuarios.utils.Constantes;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Handles requests for the application home page.
 */
@Controller
public class  TelecomUsuarioServiciosController {
	
private static final Logger LOGGER = LoggerFactory.getLogger(TelecomUsuarioServiciosController.class);

private static final String PATH_LOGIN = "/telecom/login";
	
private static final String PATH_ALTA_USUARIO = "/telecom/registro";
	
private static final String PATH_ACTUALIZA_USUARIO = "/telecom/actualiza";
        
private static final String PATH_ACTUALIZA_USUARIO_USU = "/telecom/actualizausuario";
        
private static final String PATH_ACTUALIZA_NOMBRE_USUARIO = "/telecom/actualizanombreusuario";
        
private static final String PATH_ACTUALIZA_PAIS_USUARIO = "/telecom/actualizapaisusuario";

private static final String PATH_ACTUALIZA_USUARIO_CORREO = "/telecom/actualizacorreo";
    
	private static final String PATH_ACTUALIZA_USUARIO_TELEFONO = "/telecom/actualizatelefono";
    
	private static final String PATH_RESET_PASS_USUARIO = "/telecom/resetpassusu";
    
	private static final String PATH_CHANGE_PASS_USUARIO = "/telecom/changepassusu";

	private static final String PATH_BAJA_USUARIO = "/telecom/baja";	
	
	private static final String PATH_TEST = "/telecom/test";
	
	private static final String PATH_ENCRIPTAR = "/telecom/encriptar";
	
	private static final String PARAM_JSON = "json";
	
	private static final String VIEW_HOME = "home";
	
    private static final SimpleDateFormat FORMATO_CIFRADO = new SimpleDateFormat(Constantes.FORMATO_ENCRIPT);
        
    private static Gson GSON = new Gson();
        
	@Autowired
	private UsuarioService usuarioServices;
	
	@RequestMapping(value = PATH_TEST, method=RequestMethod.GET)
	public ModelAndView home() {	
		LOGGER.info(Constantes.LOG_PROCESO_TESTING);
		ModelAndView mav = new ModelAndView(VIEW_HOME);
		return mav;	
	}
	
	@RequestMapping(value = PATH_LOGIN, method=RequestMethod.POST)
	public @ResponseBody String login(@RequestParam(PARAM_JSON) String json) {
		return usuarioServices.loginUsuario(json);
	}
	
	@RequestMapping(value = PATH_ALTA_USUARIO, method=RequestMethod.POST)
	public @ResponseBody String  guardaUsuario(@RequestParam(PARAM_JSON) String json) {
                String jsoncon = "";
                /*TUsuarios usu = new TUsuarios();
                        usu.setUsrLogin("pachoncito");
                        usu.setUsrPwd("O4c4M0sy8D");//OK
                        //usu.setUsr_pwd("58jPXOeHQ");
                        usu.setNewPassword("");
                        usu.setUsrFechaNac("14-06-1982");
                        usu.setUsrTelefono("5571986607");
                        usu.setOperador(0);
                        usu.setUsrNombre("carlos");
                        usu.setUsrApellido("sotomayor");
                        usu.setUsrDireccion("direccion nueva");
                        usu.setUsrTdcNumero("1475785874745878");
                        usu.setUsrTdcVigencia("12/19");
                        usu.setIdBanco(1);
                        usu.setIdTipoTarjeta(1);
                        usu.setIdProveedor(3);
                        usu.setIdUsrStatus(1);
                        usu.setCedula("");
                        usu.setTipoCedula(1);
                        usu.setRecibirSMS(1);
                        usu.setIdPais(2);//mexico
                        usu.setGemalto(0);
                        usu.seteMail("");
                        usu.setImei("554654654657879888");
                        usu.setTipo("iPhone");
                        usu.setSoftware("4.1.2");
                        usu.setModelo("LG-P714");
                        usu.setWkey("355737054035987");
                        usu.setTelefonoOriginal("452101965");
                        usu.setUsrMaterno("aguilar");
                        usu.setUsrSexo("M");
                        usu.setUsrTelCasa("5554878966");
                        usu.setUsrTelOficina("5554878967");
                        usu.setUsrIdEstado(16);
                        usu.setUsrCiudad("mexico");
                        usu.setUsrCalle("amado de prueba");
                        usu.setUsrNumExt(777);
                        usu.setUsrNumInterior("");
                        usu.setUsrColonia("colonia de pruebita");
                        usu.setUsrCp("54688");
                        usu.setUsrDomAmex("mismo domicilio que todos");
                        usu.setUsrTerminos("terminos y condiciones terminos y condiciones teminos y condiciones");
                        usu.setNumExtStr("");
                        usu.setIdIngo("");
                        usu.setUsrNss("");
                        jsoncon = GSON.toJson(usu);
                        LOGGER.info("jsoncon carlos: " + AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), jsoncon));
                        LOGGER.info("jsoncon carlos 1: " + jsoncon);ORMATO_CIFRADO.f
                        jsoncon = AddcelCrypto.decryptSensitive(json);        
                        LOGGER.info("json que se envia para guardar : " + jsoncon);
                        json = GSON.toJson(json);*/
                        LOGGER.info("json que se envia para guardar : " + json);
		return usuarioServices.guardarUsuario(json);
	}
	
	@RequestMapping(value = PATH_ACTUALIZA_USUARIO, method=RequestMethod.POST)
	public @ResponseBody String  actualizarUsuario(@RequestParam(PARAM_JSON) String json) {	
                LOGGER.info("json que se envia para actualizar : " + json);
		return usuarioServices.actualizarUsuario(json);
	}
        
        @RequestMapping(value = PATH_ACTUALIZA_USUARIO_USU, method=RequestMethod.POST)
	public @ResponseBody String  actualizarUsuarioUsu(@RequestParam(PARAM_JSON) String json) {	
                LOGGER.info("json que se envia para actualizar solo usuario: " + json);
		return usuarioServices.actualizarUsuarioUsu(json);
	}
        
        /*end point para actualizar nombre de usuario*/
        @RequestMapping(value = PATH_ACTUALIZA_NOMBRE_USUARIO, method=RequestMethod.POST)
	public @ResponseBody String  actualizarNombreUsu(@RequestParam(PARAM_JSON) String json) {	
                LOGGER.info("json que se envia para actualizar solo nombre usuario: " + json);
		return usuarioServices.actualizarNombreUsu(json);
	}
        
        /*end point para actualizar nombre de usuario*/
        @RequestMapping(value = PATH_ACTUALIZA_PAIS_USUARIO, method=RequestMethod.POST)
	public @ResponseBody String  actualizarPaisUsu(@RequestParam(PARAM_JSON) String json) {	
                LOGGER.info("json que se envia para actualizar solo pais usuario: " + json);
		return usuarioServices.actualizarPaisUsu(json);
	}
        
        
        @RequestMapping(value = PATH_ACTUALIZA_USUARIO_CORREO, method=RequestMethod.POST)
	public @ResponseBody String  actualizarUsuarioCorreo(@RequestParam(PARAM_JSON) String json) {	
                LOGGER.info("json que se envia para actualizar solo correo: " + json);
		return usuarioServices.actualizarCorreo(json);
	}
        
        @RequestMapping(value = PATH_ACTUALIZA_USUARIO_TELEFONO, method=RequestMethod.POST)
	public @ResponseBody String  actualizarUsuarioTelefono(@RequestParam(PARAM_JSON) String json) {	
                LOGGER.info("json que se envia para actualizar solo telefono: " + json);
		return usuarioServices.actualizarTelefono(json);
	}
	
        @RequestMapping(value = PATH_RESET_PASS_USUARIO, method=RequestMethod.POST)
	public @ResponseBody String  resetPassUsuario(@RequestParam(PARAM_JSON) String json) {	
		return usuarioServices.resetPassUsuario(json);
	}
        
        @RequestMapping(value = PATH_CHANGE_PASS_USUARIO, method=RequestMethod.POST)
	public @ResponseBody String  changePassUsuario(@RequestParam(PARAM_JSON) String json) {	
		return usuarioServices.changePassUsuario(json);
	}
        
        
//        @RequestMapping(value = PATH_ENCRIPTAR, method=RequestMethod.POST)
//    	public @ResponseBody String  encriptar(@RequestParam(PARAM_JSON) String json) {	
//    		return usuarioServices.encriptar(json);
//    	} 
	/*@RequestMapping(value = PATH_BAJA_USUARIO, method=RequestMethod.POST)
	public @ResponseBody String borrarUsuario(@RequestParam(PARAM_JSON) String jsonEnc) {
		return usuarioServices.borrarUsuario(jsonEnc);
	}*/
	
}