package com.addcel.telecom.usuarios.controller;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.telecom.usuarios.services.CatalogoService;
import com.addcel.telecom.usuarios.utils.Constantes;
import com.google.gson.Gson;

/**
 * Handles requests for the application home page.
 */
@Controller
public class TelecomCatalogoServiciosController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TelecomCatalogoServiciosController.class);

	private static final String PATH_catalogo = "/telecom/catalogo";		
			
	private static final String PARAM_JSON = "json";
	
	private static final String VIEW_HOME = "home";
	
    private static final SimpleDateFormat FORMATO_CIFRADO = new SimpleDateFormat(Constantes.FORMATO_ENCRIPT);
        
    private static Gson GSON = new Gson();

    @Autowired
	private CatalogoService catalogoServices;
	
    @RequestMapping(value = PATH_catalogo, method=RequestMethod.POST)
	public @ResponseBody String login(@RequestParam(PARAM_JSON) String json) {
    	return catalogoServices.consultarCatalogo(json);		
	}
}
