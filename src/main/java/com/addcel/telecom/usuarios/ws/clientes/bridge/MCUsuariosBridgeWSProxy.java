package com.addcel.telecom.usuarios.ws.clientes.bridge;

import java.rmi.RemoteException;

public class MCUsuariosBridgeWSProxy implements com.addcel.telecom.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_PortType {
  private String _endpoint = null;
  private com.addcel.telecom.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_PortType mCUsuariosBridgeWS_PortType = null;
  
  public MCUsuariosBridgeWSProxy() {
    _initMCUsuariosBridgeWSProxy();
  }
  
  public MCUsuariosBridgeWSProxy(String endpoint) {
    _endpoint = endpoint;
    _initMCUsuariosBridgeWSProxy();
  }
  
  private void _initMCUsuariosBridgeWSProxy() {
    try {
      mCUsuariosBridgeWS_PortType = (new com.addcel.telecom.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_ServiceLocator()).getMCUsuariosBridgeWSPort();
      if (mCUsuariosBridgeWS_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)mCUsuariosBridgeWS_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)mCUsuariosBridgeWS_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (mCUsuariosBridgeWS_PortType != null)
      ((javax.xml.rpc.Stub)mCUsuariosBridgeWS_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_PortType getMCUsuariosBridgeWS_PortType() {
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType;
  }
  
  public java.lang.String procesaTarjetaUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.procesaTarjetaUsuario(arg0);
  }
  
  public java.lang.String guardarUsuario(java.lang.String json) throws java.rmi.RemoteException{
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.guardarUsuario(json);
  }
  
  public java.lang.String actualizarUsuario(java.lang.String json) throws java.rmi.RemoteException{
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.actualizarUsuario(json);
  }
  
  public java.lang.String bajaUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.bajaUsuario(arg0);
  }
  
  public java.lang.String cambiarPassword(java.lang.String json) throws java.rmi.RemoteException{
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.cambiarPassword(json);
  }
  
  public java.lang.String resetPassword(java.lang.String json) throws java.rmi.RemoteException{
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.resetPassword(json);
  }
  
  public java.lang.String cambiarTarjeta(java.lang.String arg0) throws java.rmi.RemoteException{
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.cambiarTarjeta(arg0);
  }
  
  public java.lang.String consultaCatalogos(java.lang.String arg0) throws java.rmi.RemoteException{
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.consultaCatalogos(arg0);
  }
  
  public java.lang.String login(java.lang.String json) throws java.rmi.RemoteException{
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.login(json);
  }

    public String actualizarTelefono(String json) throws RemoteException {
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.actualizarTelefono(json);   
    }

    public String actualizarUsrLogin(String json) throws RemoteException {
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.actualizarUsrLogin(json); 
    }

    public String actualizarCorreo(String json) throws RemoteException {
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.actualizarCorreo(json); 
    }
    
    public String actualizarNombreUsu(String json) throws RemoteException {
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.actualizarNombreUsu(json); 
    }
  
    public String actualizarPaisUsu(String json) throws RemoteException {
    if (mCUsuariosBridgeWS_PortType == null)
      _initMCUsuariosBridgeWSProxy();
    return mCUsuariosBridgeWS_PortType.actualizarPaisUsu(json); 
    }   
  
}