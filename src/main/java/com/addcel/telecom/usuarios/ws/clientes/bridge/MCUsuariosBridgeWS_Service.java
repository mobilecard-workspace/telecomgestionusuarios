/**
 * MCUsuariosBridgeWS_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.bridge;

public interface MCUsuariosBridgeWS_Service extends javax.xml.rpc.Service {
    public java.lang.String getMCUsuariosBridgeWSPortAddress();

    public com.addcel.telecom.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_PortType getMCUsuariosBridgeWSPort() throws javax.xml.rpc.ServiceException;

    public com.addcel.telecom.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_PortType getMCUsuariosBridgeWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
