/**
 * MCUsuariosBridgeWS_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.bridge;

import java.rmi.RemoteException;

public interface MCUsuariosBridgeWS_PortType extends java.rmi.Remote {
    public java.lang.String login(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String actualizarTelefono(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String actualizarUsrLogin(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String actualizarUsuario(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String cambiarPassword(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String resetPassword(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String guardarUsuario(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String actualizarCorreo(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String actualizarPaisUsu(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String actualizarNombreUsu(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String procesaTarjetaUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String consultaCatalogos(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String cambiarTarjeta(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String bajaUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    
}
