package com.addcel.telecom.usuarios.model.vo;

import java.io.Serializable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestUrlAplicacionModuloVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1312157953408043425L;
	
	private int idAplicacion;
	
	private int modulo;
	
	private int plataforma;
	
	private String imagen;
	
	private String urlTienda;
	
	public int getIdAplicacion() {
		return idAplicacion;
	}
	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}
	public int getModulo() {
		return modulo;
	}
	public void setModulo(int modulo) {
		this.modulo = modulo;
	}
	public int getPlataforma() {
		return plataforma;
	}
	public void setPlataforma(int plataforma) {
		this.plataforma = plataforma;
	}
	public String getUrlTienda() {
		return urlTienda;
	}
	public void setUrlTienda(String urlTienda) {
		this.urlTienda = urlTienda;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	
}
