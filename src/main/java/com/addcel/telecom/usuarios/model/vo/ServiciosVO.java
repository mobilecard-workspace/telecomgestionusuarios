package com.addcel.telecom.usuarios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ServiciosVO {

	private int id;
	
	private int idCiudad;

	private String alias;
	
	private int idRedebanTipo;
	
	private int codigoServicio;
	
	private String referenciaPago;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdCiudad() {
		return idCiudad;
	}

	public void setIdCiudad(int idCiudad) {
		this.idCiudad = idCiudad;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public int getIdRedebanTipo() {
		return idRedebanTipo;
	}

	public void setIdRedebanTipo(int idRedebanTipo) {
		this.idRedebanTipo = idRedebanTipo;
	}

	public int getCodigoServicio() {
		return codigoServicio;
	}

	public void setCodigoServicio(int codigoServicio) {
		this.codigoServicio = codigoServicio;
	}

	public String getReferenciaPago() {
		return referenciaPago;
	}

	public void setReferenciaPago(String referenciaPago) {
		this.referenciaPago = referenciaPago;
	}
	
}
