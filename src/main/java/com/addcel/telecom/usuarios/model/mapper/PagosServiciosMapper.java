package com.addcel.telecom.usuarios.model.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.telecom.usuarios.model.vo.BusquedaPagoVO;
import com.addcel.telecom.usuarios.model.vo.PagoVO;
import com.addcel.telecom.usuarios.model.vo.PagosResponseVO;
import com.addcel.telecom.usuarios.model.vo.RecargaTransaccionVO;
import com.addcel.telecom.usuarios.model.vo.RecargaVO;
import com.addcel.telecom.usuarios.model.vo.ServiciosRequestVO;

public interface PagosServiciosMapper {

	public String getFechaActual();

	public int difFechaMin(String token);
		
	public List<PagosResponseVO> busquedaPago(BusquedaPagoVO busq);

	public RecargaVO getInfoRecarga(PagoVO pago);

	public List<RecargaVO> getMontosRecargas(ServiciosRequestVO request);

	public void guardaTransaccionMovilRed(RecargaTransaccionVO prepaidSale);

	public void getServicios(@Param(value = "map") HashMap<String, String> map);
	
}
