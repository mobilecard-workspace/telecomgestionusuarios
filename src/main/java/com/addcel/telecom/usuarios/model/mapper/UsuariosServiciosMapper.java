package com.addcel.telecom.usuarios.model.mapper;


import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.telecom.usuarios.model.vo.ProcessTagsVO;
import com.addcel.telecom.usuarios.model.vo.TUsuarios;



public interface UsuariosServiciosMapper {
	
    public String guardarUsuario(TUsuarios usuario);
    public String ProcessCard(HashMap parameters);
    public String loginUsuario(TUsuarios usuario);
    public String getParameter(@Param(value = "parameter") String parameter);
  //  public void userActivate(@Param(value = "user") String user, @Param(value = "pass") String pass);
    public String userActivate(@Param(value = "user") String user, @Param(value = "pass") String pass, @Param(value = "idioma") String idioma);
    public String cambiarPasswordUsuario(TUsuarios usuario);
    public String resetPasswordUsuario(TUsuarios usuario);
    //public void updateUserInfo(@Param(value = "name") String name, @Param(value = "value") String value, @Param(value = "idUsuario") long idUsuario);
    public String updateUserInfo(@Param(value = "idioma") String idioma,@Param(value = "name") String name, @Param(value = "value") String value, @Param(value = "idUsuario") long idUsuario);
    public TUsuarios getUserInfo(@Param(value = "idUsuario") long idUsuario);
    public String processTags(ProcessTagsVO processTags);
    public String getTerminos(@Param(value = "idPais") int idPais, @Param(value = "idioma") String idioma);

}