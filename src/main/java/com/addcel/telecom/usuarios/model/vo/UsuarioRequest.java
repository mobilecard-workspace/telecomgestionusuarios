/**
 * T
ipoSolicitudCompra.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class UsuarioRequest {
	
	private long idUsuario;
	private String login;
	private String password;
	private String newPassword;
	private String telefono;
	private String telefonoCon;
	private String operador;
	private String email;
	private String emailCon;
	private String nombres;
	private String apellidoPrim;
	private String apellidoSeg;
	private String fechaNacimiento;
	private String genero;
	private String telefonoCasa;
	private String telefonoOficina;
	private String pais;
	private String direccion;
	private String direccionCom;
	private String codigoPostal;
	
	private String tarjeta;
	private String vigencia;
	private String tarjetaCif;
	
	private String plataforma;
	private String dispositivo;
	private int status;
	private int idAplicacion;
	private int sms;
	private int terminos;
	
	private String respuesta;
	
	private String cedula;
	private int tipoCedula;
	private String idUsuarioComercio;
	
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getOperador() {
		return operador;
	}
	public void setOperador(String operador) {
		this.operador = operador;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidoPrim() {
		return apellidoPrim;
	}
	public void setApellidoPrim(String apellidoPrim) {
		this.apellidoPrim = apellidoPrim;
	}
	public String getApellidoSeg() {
		return apellidoSeg;
	}
	public void setApellidoSeg(String apellidoSeg) {
		this.apellidoSeg = apellidoSeg;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getTelefonoCasa() {
		return telefonoCasa;
	}
	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}
	public String getTelefonoOficina() {
		return telefonoOficina;
	}
	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getPlataforma() {
		return plataforma;
	}
	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}
	public String getDispositivo() {
		return dispositivo;
	}
	public void setDispositivo(String dispositivo) {
		this.dispositivo = dispositivo;
	}
	public int getIdAplicacion() {
		return idAplicacion;
	}
	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}
	public int getSms() {
		return sms;
	}
	public void setSms(int sms) {
		this.sms = sms;
	}
	public int getTerminos() {
		return terminos;
	}
	public void setTerminos(int terminos) {
		this.terminos = terminos;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getTarjetaCif() {
		return tarjetaCif;
	}
	public void setTarjetaCif(String tarjetaCif) {
		this.tarjetaCif = tarjetaCif;
	}
	public String getTelefonoCon() {
		return telefonoCon;
	}
	public void setTelefonoCon(String telefonoCon) {
		this.telefonoCon = telefonoCon;
	}
	public String getEmailCon() {
		return emailCon;
	}
	public void setEmailCon(String emailCon) {
		this.emailCon = emailCon;
	}
	public String getDireccionCom() {
		return direccionCom;
	}
	public void setDireccionCom(String direccionCom) {
		this.direccionCom = direccionCom;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public int getTipoCedula() {
		return tipoCedula;
	}
	public void setTipoCedula(int tipoCedula) {
		this.tipoCedula = tipoCedula;
	}
	public String getIdUsuarioComercio() {
		return idUsuarioComercio;
	}
	public void setIdUsuarioComercio(String idUsuarioComercio) {
		this.idUsuarioComercio = idUsuarioComercio;
	}
	
}
