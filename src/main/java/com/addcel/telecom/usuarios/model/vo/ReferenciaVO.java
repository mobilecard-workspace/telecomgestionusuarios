package com.addcel.telecom.usuarios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ReferenciaVO extends AbstractVO{
	
	private String referencia;
	
	private String descripcion;
	
	private double montoTotal;
	
	private double montoIva;

	public ReferenciaVO() {
	}
	
	public ReferenciaVO(String referencia, String descripcion, double montoTotal, double montoIva) {
		this.referencia = referencia;
		this.descripcion = descripcion;
		this.montoTotal = montoTotal;
		this.montoIva = montoIva;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(double montoTotal) {
		this.montoTotal = montoTotal;
	}

	public double getMontoIva() {
		return montoIva;
	}

	public void setMontoIva(double montoIva) {
		this.montoIva = montoIva;
	}

}
