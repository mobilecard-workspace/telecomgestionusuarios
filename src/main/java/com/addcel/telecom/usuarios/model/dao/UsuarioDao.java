package com.addcel.telecom.usuarios.model.dao;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.telecom.usuarios.model.vo.UsuariosUsa;
import com.addcel.telecom.usuarios.utils.Constantes;

import org.slf4j.LoggerFactory;

public class UsuarioDao extends SqlMapClientDaoSupport{
		
	private static final Logger LOG = LoggerFactory.getLogger(UsuarioDao.class);
	
	public int guardarUsuario(UsuariosUsa usuario){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_PROCESO_ALTA_USUARIO + usuario.getUsuario());
			getSqlMapClientTemplate().queryForObject("guardarUsuario", usuario);
			LOG.info(Constantes.LOG_RESPUESTA + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.ERROR_INESPERADO_ALTA_USUARIO + usuario.getUsuario(), e);
		}
		return resp;
	}
	
	/*public int actualizarUsuario(UsuarioRequest usuario){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_ACTUALIZAR_USUARIO  + usuario.getLogin());
			getSqlMapClientTemplate().queryForObject("actualizarUsuario", usuario);
			LOG.info(Constantes.LOG_RESP_DAO_ACTUALIZAR_USUARIO + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_ACTUALIZAR_USUARIO + usuario.getLogin(), e);
		}
		return resp;
	}*/
	
		
}