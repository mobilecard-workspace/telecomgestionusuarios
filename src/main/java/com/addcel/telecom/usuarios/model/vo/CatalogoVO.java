package com.addcel.telecom.usuarios.model.vo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CatalogoVO extends AbstractVO{
	
	private List<AbstractLabelValue> generos;
	
	private List<AbstractLabelValue> paises;
	
	private List<AbstractLabelValue> operador;
	
	private List<AbstractLabelValue> documentos;
	
	private String terminos;
	
	private int idTermino;
	
	private List<AvisosVO> avisos;

	public List<AbstractLabelValue> getGeneros() {
		return generos;
	}

	public void setGeneros(List<AbstractLabelValue> generos) {
		this.generos = generos;
	}

	public List<AbstractLabelValue> getPaises() {
		return paises;
	}

	public void setPaises(List<AbstractLabelValue> paises) {
		this.paises = paises;
	}

	public List<AbstractLabelValue> getOperador() {
		return operador;
	}

	public void setOperador(List<AbstractLabelValue> operador) {
		this.operador = operador;
	}

	public String getTerminos() {
		return terminos;
	}

	public void setTerminos(String terminos) {
		this.terminos = terminos;
	}

	public List<AvisosVO> getAvisos() {
		return avisos;
	}

	public void setAvisos(List<AvisosVO> avisos) {
		this.avisos = avisos;
	}

	public int getIdTermino() {
		return idTermino;
	}

	public void setIdTermino(int idTermino) {
		this.idTermino = idTermino;
	}

	public List<AbstractLabelValue> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(List<AbstractLabelValue> documentos) {
		this.documentos = documentos;
	}

	
}
