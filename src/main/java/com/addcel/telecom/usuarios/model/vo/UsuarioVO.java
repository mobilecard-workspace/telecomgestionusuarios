/**
 * TipoSolicitudCompra.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class UsuarioVO extends UsuarioRequest{
	
	private String operadorDesc;
	private String generoDesc;
	private int franquicia;
	private String franquiciaDesc;
	private int idError;
	private String mensajeError;
	private String paisDesc;
	
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	public String getOperadorDesc() {
		return operadorDesc;
	}
	public void setOperadorDesc(String operadorDesc) {
		this.operadorDesc = operadorDesc;
	}
	public String getGeneroDesc() {
		return generoDesc;
	}
	public void setGeneroDesc(String generoDesc) {
		this.generoDesc = generoDesc;
	}
	public int getFranquicia() {
		return franquicia;
	}
	public void setFranquicia(int franquicia) {
		this.franquicia = franquicia;
	}
	public String getFranquiciaDesc() {
		return franquiciaDesc;
	}
	public void setFranquiciaDesc(String franquiciaDesc) {
		this.franquiciaDesc = franquiciaDesc;
	}
	public String getPaisDesc() {
		return paisDesc;
	}
	public void setPaisDesc(String paisDesc) {
		this.paisDesc = paisDesc;
	}


}
