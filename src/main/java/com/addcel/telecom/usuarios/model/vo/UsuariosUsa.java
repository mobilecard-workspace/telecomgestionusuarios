package com.addcel.telecom.usuarios.model.vo;

import java.util.Date;

public class UsuariosUsa {
    private Integer idusuario;

    private Integer genero;

    private String usuario;

    private String clave;

    private String nombre;

    private String primerapellido;

    private String segundoapellido;

    private String celular;

    private String operador;

    private String email;

    private String cedula;

    private Integer tipocedula;

    private Date fechanacimiento;

    private String telefonoresidencia;

    private String telefonooficina;

    private Date fecharegistro;

    private Integer recibirSMS;

    private Integer terminos;

    private Integer estado;

    private Integer idpais;

    private String direccion;

    private Integer gemalto;
    
    private String respuesta;

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Integer getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    public Integer getGenero() {
        return genero;
    }

    public void setGenero(Integer genero) {
        this.genero = genero;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerapellido() {
        return primerapellido;
    }

    public void setPrimerapellido(String primerapellido) {
        this.primerapellido = primerapellido;
    }

    public String getSegundoapellido() {
        return segundoapellido;
    }

    public void setSegundoapellido(String segundoapellido) {
        this.segundoapellido = segundoapellido;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Integer getTipocedula() {
        return tipocedula;
    }

    public void setTipocedula(Integer tipocedula) {
        this.tipocedula = tipocedula;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getTelefonoresidencia() {
        return telefonoresidencia;
    }

    public void setTelefonoresidencia(String telefonoresidencia) {
        this.telefonoresidencia = telefonoresidencia;
    }

    public String getTelefonooficina() {
        return telefonooficina;
    }

    public void setTelefonooficina(String telefonooficina) {
        this.telefonooficina = telefonooficina;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public Integer getRecibirSMS() {
        return recibirSMS;
    }

    public void setRecibirSMS(Integer recibirSMS) {
        this.recibirSMS = recibirSMS;
    }

    public Integer getTerminos() {
        return terminos;
    }

    public void setTerminos(Integer terminos) {
        this.terminos = terminos;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getIdpais() {
        return idpais;
    }

    public void setIdpais(Integer idpais) {
        this.idpais = idpais;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getGemalto() {
        return gemalto;
    }

    public void setGemalto(Integer gemalto) {
        this.gemalto = gemalto;
    }
}