package com.addcel.telecom.usuarios.model.vo;

public class RecargaTransaccionVO {

	private double monto;
	
	private String productoMovilRed;
	
	private String destino;
	
	private String sesion;
	
	private String fechaTransaccion;
	
	private String codigoRespuesta;
	
	private String idTransaccionMovilRed;
	
	private String codigoErrorMovilRed;
	
	private String descripcionErrorMovilRed;

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getProductoMovilRed() {
		return productoMovilRed;
	}

	public void setProductoMovilRed(String productoMovilRed) {
		this.productoMovilRed = productoMovilRed;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getSesion() {
		return sesion;
	}

	public void setSesion(String sesion) {
		this.sesion = sesion;
	}

	public String getFechaTransaccion() {
		return fechaTransaccion;
	}

	public void setFechaTransaccion(String fechaTransaccion) {
		this.fechaTransaccion = fechaTransaccion;
	}

	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getIdTransaccionMovilRed() {
		return idTransaccionMovilRed;
	}

	public void setIdTransaccionMovilRed(String idTransaccionMovilRed) {
		this.idTransaccionMovilRed = idTransaccionMovilRed;
	}

	public String getCodigoErrorMovilRed() {
		return codigoErrorMovilRed;
	}

	public void setCodigoErrorMovilRed(String codigoErrorMovilRed) {
		this.codigoErrorMovilRed = codigoErrorMovilRed;
	}

	public String getDescripcionErrorMovilRed() {
		return descripcionErrorMovilRed;
	}

	public void setDescripcionErrorMovilRed(String descripcionErrorMovilRed) {
		this.descripcionErrorMovilRed = descripcionErrorMovilRed;
	}
	
}
