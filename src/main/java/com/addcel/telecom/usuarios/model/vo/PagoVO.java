package com.addcel.telecom.usuarios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PagoVO extends AbstractVO {
	
	private int id;
	
	private long idUsuario;
	
	private String login;
	
	private String password;
	
	private String cvv2;
	
	private String CVV;
	
	private String telefono;
	
	private String software;
	
	private String modelo;
	
	private String imei;
	
	private String tipo;
	
	private double cx;
	
	private double cy;
	
	private String token;
	
	private long idTransaccionPago;
	
	private String descripcion;
	
	private String codigoRespuesta;
	
	private int estado;
	
	private int idAplicacion;
	
	private String noAprobacion;
	
	private int idProducto;
	
	private int monto; 
	
	private int idRecarga;
	
	private String operador;
	
	private String productMovilRed;
	
	private String productTypeMovilRed;
	
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getIdTransaccionPago() {
		return idTransaccionPago;
	}
	public void setIdTransaccionPago(long idTransaccionPago) {
		this.idTransaccionPago = idTransaccionPago;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public int getIdAplicacion() {
		return idAplicacion;
	}
	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}
	public String getNoAprobacion() {
		return noAprobacion;
	}
	public void setNoAprobacion(String noAprobacion) {
		this.noAprobacion = noAprobacion;
	}
	public int getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public double getCx() {
		return cx;
	}
	public void setCx(double cx) {
		this.cx = cx;
	}
	public double getCy() {
		return cy;
	}
	public void setCy(double cy) {
		this.cy = cy;
	}
	public int getMonto() {
		return monto;
	}
	public void setMonto(int monto) {
		this.monto = monto;
	}
	public int getIdRecarga() {
		return idRecarga;
	}
	public void setIdRecarga(int idRecarga) {
		this.idRecarga = idRecarga;
	}
	public String getOperador() {
		return operador;
	}
	public void setOperador(String operador) {
		this.operador = operador;
	}
	public String getProductMovilRed() {
		return productMovilRed;
	}
	public void setProductMovilRed(String productMovilRed) {
		this.productMovilRed = productMovilRed;
	}
	public String getProductTypeMovilRed() {
		return productTypeMovilRed;
	}
	public void setProductTypeMovilRed(String productTypeMovilRed) {
		this.productTypeMovilRed = productTypeMovilRed;
	}
	public String getCVV() {
		return CVV;
	}
	public void setCVV(String CVV) {
		this.CVV = CVV;
	}

}
