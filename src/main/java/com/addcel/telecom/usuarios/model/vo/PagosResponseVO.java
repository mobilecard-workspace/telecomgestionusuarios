package com.addcel.telecom.usuarios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PagosResponseVO {

	private int idTransaccion;
	
	private int idServicio;
	
	private String referencia;
	
	private double total;
	
	private double iva;
	
	private String status;
	
	private String fecha;
	
	private String tipoPago;
	
	private String descripcion; 


	public int getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(int idTranscaccion) {
		this.idTransaccion = idTranscaccion;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public String getStatus() {
		return status;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	
}
