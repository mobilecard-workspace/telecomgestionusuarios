package com.addcel.telecom.usuarios.model.vo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ServiciosRequestVO extends AbstractVO{

	private int idAplicacion;
	
	private int idUsuario;
	
	private String usuario;
	
	private String password;
	
	private String referencia;
	
	private int idServicio;
	
	private String referenciaServicio;
	
	private int idProveedor; 
	
	private int idProducto;
	
	private List<RecargaVO> recargas;

	public int getIdAplicacion() {
		return idAplicacion;
	}

	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public int getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}

	public String getReferenciaServicio() {
		return referenciaServicio;
	}

	public void setReferenciaServicio(String referenciaServicio) {
		this.referenciaServicio = referenciaServicio;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public List<RecargaVO> getRecargas() {
		return recargas;
	}

	public void setRecargas(List<RecargaVO> recargas) {
		this.recargas = recargas;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	
}
