package com.addcel.telecom.usuarios.model.vo;

public class RecargaVO{

	private int idRecarga;
	
	private int idProducto;
	
	private int monto;
	
	private String descripcion;
	
	private double comision;
	
	private int idOperador;

	private String operador;
	
	private String productMovilRed;
	
	private String productTypeMovilRed;
	
	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public int getMonto() {
		return monto;
	}

	public void setMonto(int monto) {
		this.monto = monto;
	}

	public int getIdRecarga() {
		return idRecarga;
	}

	public void setIdRecarga(int idRecarga) {
		this.idRecarga = idRecarga;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public int getIdOperador() {
		return idOperador;
	}

	public void setIdOperador(int idOperador) {
		this.idOperador = idOperador;
	}

	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	public String getProductMovilRed() {
		return productMovilRed;
	}

	public void setProductMovilRed(String productMovilRed) {
		this.productMovilRed = productMovilRed;
	}

	public String getProductTypeMovilRed() {
		return productTypeMovilRed;
	}

	public void setProductTypeMovilRed(String productTypeMovilRed) {
		this.productTypeMovilRed = productTypeMovilRed;
	}
	
}
