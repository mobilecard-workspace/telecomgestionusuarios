package com.addcel.telecom.usuarios.model.vo;


public class ServicioTaeRespuesta {

//	{"status":1,"error":52,"descripcion":"Numero celular no valido"}
//	{"status":0,"id":"1549","descripcion":"Recarga recibida con exito"}
	
	int status;
	
	int id;
	
	int error;
	
	String descripcion;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getError() {
		return error;
	}

	public void setError(int error) {
		this.error = error;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
