package com.addcel.telecom.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VerifyResponse  extends BaseResponse{
	
	 private String usr_telefono; 
	 private String usr_nombre;
	 private String usr_apellido;
	 private Integer id_usr_status;
	 private Integer idpais;
	 
	 public VerifyResponse() {
		// TODO Auto-generated constructor stub
	}
	 
	public String getUsr_telefono() {
		return usr_telefono;
	}
	public void setUsr_telefono(String usr_telefono) {
		this.usr_telefono = usr_telefono;
	}
	public String getUsr_nombre() {
		return usr_nombre;
	}
	public void setUsr_nombre(String usr_nombre) {
		this.usr_nombre = usr_nombre;
	}
	public String getUsr_apellido() {
		return usr_apellido;
	}
	public void setUsr_apellido(String usr_apellido) {
		this.usr_apellido = usr_apellido;
	}
	public Integer getId_usr_status() {
		return id_usr_status;
	}
	public void setId_usr_status(Integer id_usr_status) {
		this.id_usr_status = id_usr_status;
	}
	public Integer getIdpais() {
		return idpais;
	}
	public void setIdpais(Integer idpais) {
		this.idpais = idpais;
	}
	 
	 

}
