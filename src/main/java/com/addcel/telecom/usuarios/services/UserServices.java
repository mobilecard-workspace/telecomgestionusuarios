package com.addcel.telecom.usuarios.services;

import java.util.HashMap;

import org.apache.axis.encoding.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.telecom.usuarios.model.vo.BaseResponse;
import com.addcel.telecom.usuarios.model.mapper.UsuariosServiciosMapper;
import com.addcel.telecom.usuarios.model.vo.InsertRequest;
import com.addcel.telecom.usuarios.model.vo.LoginRequest;
import com.addcel.telecom.usuarios.model.vo.TUsuarios;
import com.addcel.telecom.usuarios.model.vo.TermResponse;
import com.addcel.telecom.usuarios.model.vo.UpdateResponse;
import com.addcel.telecom.usuarios.model.vo.VerifyResponse;
import com.addcel.telecom.usuarios.utils.Constantes;
import com.addcel.telecom.usuarios.utils.UtilsMC;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

import crypto.Crypto;

@Service
public class UserServices {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServices.class);
	@Autowired
	private UsuariosServiciosMapper mapper;
    private Gson gson = new Gson();
    
    
    public TUsuarios userLogin2(LoginRequest request, String idioma){
    	TUsuarios response = new TUsuarios();
    	try{
    		LOGGER.info("request.getUsrPwd(): "+request.getUsrPwd());
    		String pass_encriptado = AddcelCrypto.encryptHard(request.getUsrPwd());
    		String pass_decrypt =  AddcelCrypto.decryptHard(pass_encriptado);
    		LOGGER.info("pass_encriptado: "+pass_encriptado);
    		
    		String passmx = UtilsMC.generaSemillaAux(pass_decrypt);
    		String pass_AES =  Crypto.aesEncrypt(passmx, pass_decrypt);
    		LOGGER.info("pass_AES: "+pass_AES);
    		return response;
    		
    	}catch(Exception ex){
    		LOGGER.error("Error al insertar usuario", ex);
    		ex.printStackTrace();
    		response.setIdError(101);
    		response.setMensajeError("An error occurred, please try again.");
    		return response;
    	}
	}
    
    
    
    public TUsuarios UserInsert(InsertRequest insertRequest, String idioma){
    	
    	TUsuarios response = new TUsuarios();
    	try{
    		
    		LOGGER.debug("INICIANDO PROCESO DE ALTA USUARIO");
    		LOGGER.debug("Json: " + gson.toJson(insertRequest));
    		String pass_decrypt;
    		TUsuarios usuario  =  new TUsuarios();
    		usuario.seteMail(insertRequest.getEmail());
//    		LOGGER.info("pass_decrypt:  "+AddcelCrypto.encryptHard(insertRequest.getPassword()));
    		pass_decrypt =  AddcelCrypto.decryptHard(insertRequest.getPassword());
    		LOGGER.info("pass_decrypt:  "+pass_decrypt);
    		
    		String passmx = UtilsMC.generaSemillaAux(pass_decrypt);
    		LOGGER.info("passmx:  "+passmx);
    		String pass_AES =  Crypto.aesEncrypt(passmx, pass_decrypt);
    		
    		LOGGER.info("pass_AES:  "+pass_AES);
    		
    		usuario.setUsrLogin(insertRequest.getEmail());
    		usuario.setUsrPwd(pass_AES);
    		usuario.setUsrNombre(insertRequest.getFirstName());
    		usuario.setUsrApellido(insertRequest.getLastName());
    		usuario.setUsrTelefono(insertRequest.getPhone());
    		usuario.setIdPais(insertRequest.getCountry());
    		usuario.setImei(insertRequest.getImei());
    		usuario.setSoftware(insertRequest.getOs());
    		usuario.setModelo(insertRequest.getPlatform());
    		usuario.setIdUsrStatus(99);
    		usuario.setIdioma(idioma);
    		
//    		LOGGER.info("insertRequest.getCard().getPan() :"+AddcelCrypto.encryptHard("4110186001207775"));
//    		
//    		LOGGER.info("insertRequest.getCard().getVigencia()() :"+AddcelCrypto.encryptHard(insertRequest.getCard().getVigencia()));
//    		LOGGER.info("insertRequest.getCard().getCodigo() :"+AddcelCrypto.encryptHard(insertRequest.getCard().getCodigo()));
    		
    		LOGGER.debug("INICIANDO PROCESO DE ALTA USUARIO 2");
    		if(insertRequest.getCard() != null && insertRequest.getCard().getPan() != null )
    		{
    			usuario.setUsrTdcNumero( UtilsMC.setSMS(AddcelCrypto.decryptHard( insertRequest.getCard().getPan())));
    			usuario.setUsrTdcVigencia( UtilsMC.setSMS(AddcelCrypto.decryptHard(insertRequest.getCard().getVigencia())));
	    		usuario.setUsrTdcCodigo( UtilsMC.setSMS(AddcelCrypto.decryptHard(insertRequest.getCard().getCodigo())));
    			
    			LOGGER.debug("INICIANDO PROCESO DE ALTA USUARIO3");
    		}
    		
    		
    		usuario =  (TUsuarios) UtilsMC.validarCampos(usuario);
    		
    		
    		String responseDB = mapper.guardarUsuario(usuario);
    		
    		LOGGER.debug("[RESPONSE USER INSERT] " + responseDB);
    		
    		 response = gson.fromJson(responseDB, TUsuarios.class);
    		 
    		 //Enviar email para confirmar cuenta
    		 if(response.getIdError() == 0){
//    			 String value =  AddcelCrypto.encryptHard(insertRequest.getEmail())+":"+AddcelCrypto.encryptHard(pass_AES);
    			 String value =  AddcelCrypto.encryptHard(insertRequest.getEmail())+":"+AddcelCrypto.encryptHard(insertRequest.getEmail());
    			 String param = "?codigo="+Base64.encode(value.getBytes());
    			 String url =  Constantes.ACTIVATION_URL.replace("{idioma}", idioma)+param;
    			 LOGGER.debug("[ACTIVATE URL] " + url);
    			 if(idioma.equalsIgnoreCase("es"))    			    
    			 	UtilsMC.emailSend(insertRequest.getEmail(), mapper.getParameter("@MENSAJE_ACTIVATE_TELECOM_ES"), mapper.getParameter("@ASUNTO_ACTIVATE_TELECOM_ES"), insertRequest.getEmail(), url, "", insertRequest.getFirstName()+ " " + insertRequest.getLastName(),new String[]{Constantes.Path_images+"user_registry.PNG"});
    			 else
    				 UtilsMC.emailSend(insertRequest.getEmail(), mapper.getParameter("@MENSAJE_ACTIVATE_TELECOM_EN"), mapper.getParameter("@ASUNTO_ACTIVATE_TELECOM_EN"), insertRequest.getEmail(), url,"", insertRequest.getFirstName()+ " " + insertRequest.getLastName(),new String[]{Constantes.Path_images+"user_registry.PNG"});    				 
    		 }
    		 
    		 
    		 //insertar tarjeta
    		 if(response.getIdError() == 0 && insertRequest.getCard() != null){
    			    LOGGER.debug("[SE DETECTO TARJETA. INSERTANDO ...]");
    			 	HashMap parameters = new HashMap<>();
    				parameters.put("nusuario", insertRequest.getEmail());
    				parameters.put("nclave",pass_AES );
    				parameters.put("nnumtarjeta", UtilsMC.getSMS(usuario.getUsrTdcNumero()));
    				parameters.put("nnumtarjetacrip", usuario.getUsrTdcNumero());
    				parameters.put("nvigencia", usuario.getUsrTdcVigencia());
    				parameters.put("nproceso", 1);
    				parameters.put("nidtarjetausuario", 1);
    				parameters.put("nct", usuario.getUsrTdcCodigo());
    				parameters.put("ntarjeta", insertRequest.getFirstName().trim() + " " + insertRequest.getLastName().trim());
    				parameters.put("domamex", insertRequest.getCard().getDomAmex() );
    				parameters.put("cpamex", insertRequest.getCard().getCpAmex());
    				parameters.put("idioma", idioma);
    				parameters.put("telecomcard", 0);
    				parameters.put("tipotarjeta",  insertRequest.getCard().getTipoTarjeta().toUpperCase());
    				
    				String responseAddCard = mapper.ProcessCard(parameters);
    				LOGGER.debug("[RESPONSE INSERT CARD] " + responseAddCard);
    			 
    		 }
    		 else
    		 {
    			 LOGGER.debug("No se detecto tarjeta");
    			 if(insertRequest.getCountry() == 3)
    	        	{
    				 	TUsuarios login = gson.fromJson(mapper.loginUsuario(usuario), TUsuarios.class);
    		        	response.setIdError(90);
    		        	response.setIdUsuario(login.getIdeUsuario());
    		        	response.setIdPais(insertRequest.getCountry());
    		        	if(idioma.equals("es"))
    		        		response.setMensajeError("Desea obtener tarjeta Telecom");
    		        	else
    		        		response.setMensajeError("Want to get Telecom card");
    	        	}
    		 }
    		 LOGGER.debug("Retornando respuesta...");
			 return response;
    		
    	}catch(Exception ex){
    		LOGGER.error("Error al insertar usuario", ex);
			ex.printStackTrace();
			response.setIdError(101);
			response.setMensajeError("An error occurred, please try again.");
			return response;
		}
    	
    }
    
    
    public ModelAndView userActivate(String codigo, String idioma){
    	ModelAndView view = new ModelAndView();
    	
    	try{
    		String[] data = new String(Base64.decode(codigo)).split(":");
    		String user = AddcelCrypto.decryptHard(data[0]);
    		String pass = AddcelCrypto.decryptHard(data[1]);
    		LOGGER.debug("user = " + user);
    		LOGGER.debug("pass = " + pass);
    		//mapper.userActivate(user, pass);
    		
    		String responseJSON = mapper.userActivate(user, pass, idioma);
    		BaseResponse responseDB= gson.fromJson(responseJSON, BaseResponse.class);
    		
    		view.setViewName("activate");
    		
    		view.addObject("message", responseDB.getMensajeError());
    		return view;
    	}catch(Exception ex){
    		LOGGER.error("[Error al activar usuario] ", ex);
    		view.setViewName("activate_error");
    		view.addObject("message", "Error al activar cuenta");
    		return view;
    	}
    }
    
 public BaseResponse ActivationSendMail(String idioma, long idUsuario){
    	
    	BaseResponse response = new BaseResponse();
    	response.setIdError(0);
    	TUsuarios user = null;
    	try{
    		user = mapper.getUserInfo(idUsuario);
    		 //Enviar email para confirmar cuenta
   		 if(user.geteMail()!= null && !user.geteMail().isEmpty()){
   			 String value =  AddcelCrypto.encryptHard(user.geteMail())+":"+AddcelCrypto.encryptHard(user.geteMail());
   			 String param = "?codigo="+Base64.encode(value.getBytes());
   			 String url =  Constantes.ACTIVATION_URL.replace("{idioma}", idioma)+param;
   			 LOGGER.debug("[ACTIVATE URL] " + url);
   			 if(idioma.equalsIgnoreCase("es"))
			    UtilsMC.emailSend(user.geteMail(), mapper.getParameter("@MENSAJE_ACTIVATE_TELECOM_ES"), mapper.getParameter("@ASUNTO_ACTIVATE_ES"), user.getUsrLogin(), url, "", user.getUsrNombre()+ " " + user.getUsrApellido(), new String[]{Constantes.Path_images+"user_activation.PNG"});
   			 else
   				UtilsMC.emailSend(user.geteMail(), mapper.getParameter("@MENSAJE_ACTIVATE_TELECOM_EN"), mapper.getParameter("@ASUNTO_ACTIVATE_EN"), user.getUsrLogin(), url,"", user.getUsrNombre()+ " " + user.getUsrApellido(),new String[]{Constantes.Path_images+"user_activation.PNG"});
   		 }
    		
    	}catch(Exception ex){
    		LOGGER.error("[ERROR AL ENVIAR CORREO DE ACTIVACION ] " + idUsuario , ex);
    		response.setIdError(101);
    		if(idioma.equalsIgnoreCase("es"))
    			response.setMensajeError("Error inesperado, intente de nuevo");
    		else
    			response.setMensajeError("Unexpected error, try again");
    	}
    	
    	if(idioma.equalsIgnoreCase("es"))
			response.setMensajeError("Se envió un correo para confirmar su cuenta");
		else
			response.setMensajeError("An email was sent to confirm your account");
    	
    	return response;
    	
    }
    
    public TUsuarios userLogin(LoginRequest request, String idioma){
    	
    	LOGGER.debug("[INICIANDO LOGIN USUARIO] " + gson.toJson(request));
    	
    	TUsuarios user = new TUsuarios();
    	try{
	    	String passdecrypt = AddcelCrypto.decryptHard(request.getUsrPwd());
	    	LOGGER.info("passdecrypt:  "+passdecrypt);
	    	
	    	String passmx = UtilsMC.generaSemillaAux(passdecrypt);
	    	user.setUsrLogin(request.getUsrLoginOrEmail());
	    	
	    	user.setUsrPwd(Crypto.aesEncrypt(passmx, passdecrypt));
	    	LOGGER.info("ENCRIPTADO: "+Crypto.aesEncrypt(passmx, passdecrypt));
	    	user.setIdioma(idioma);
	    	
	    	String responseJson = mapper.loginUsuario(user);
	    	
	    	LOGGER.info("responseJson:  "+responseJson);
	    	
	    	user = gson.fromJson(responseJson, TUsuarios.class);
	    	
	    	if(user.getIdError() == 0){
	    		if(user.getUsrTdcNumero()!= null  && !user.getUsrTdcNumero().isEmpty()){
	    			String card = UtilsMC.getSMS(user.getUsrTdcNumero());
	    			user.setUsrTdcNumero(UtilsMC.maskCard(card, "*"));
	    		}
	    		user.setUsrPwd("");
	    	}
	    	
	    	
    	}catch(Exception ex){
    		LOGGER.debug("[ERROR AL LOGUEAR USUARIO] ", ex);
    		user.setIdError(101);
    		if(idioma.equalsIgnoreCase("es"))
    			user.setMensajeError("Error inesperado, intente de nuevo");
    		else
    			user.setMensajeError("Unexpected error, try again");
    	}
    	LOGGER.debug("[FINALIZANDO LOGIN USUARIO] ");
    	return user;
    }
    
    public VerifyResponse statusVerify(long idUsuario, String idioma){
    	VerifyResponse response = new VerifyResponse();
    	try{
    		
    		TUsuarios user = mapper.getUserInfo(idUsuario);
    		
    		response.setId_usr_status(user.getIdUsrStatus());
    		response.setIdpais(user.getIdPais());
    		response.setUsr_apellido(user.getUsrApellido());
    		response.setUsr_nombre(user.getUsrNombre());
    		response.setUsr_telefono(user.getUsrTelefono());
    		response.setIdError(0);
    		response.setMensajeError("");
    		
    	}catch(Exception ex){
    		LOGGER.error("[ERROR AL VERIFICAR ESTATUS ] [USUARIO " + idUsuario + "]", ex);
    		getBaseResponse(idioma, response, 99);
    	}
    	return response;
    }
    
    public BaseResponse passwordUpdate( String idioma,  String idUsuario,  String pass_old,  String pass_new){
    	
    	LOGGER.debug("[INICIANDO ACATUALIZACION DE PASSWORD ] USER: " + idUsuario );
    	TUsuarios user = new TUsuarios();
    	BaseResponse response = new BaseResponse();
    	try{
    		
    		String password =  AddcelCrypto.decryptHard(pass_new);
    		LOGGER.debug("password: "+password);
    		String passaux = UtilsMC.generaSemillaAux(password);
    	    String passnew = Crypto.aesEncrypt(passaux, password);
    		user.setNewPassword(passnew);
    		user.setIdeUsuario(Long.parseLong(idUsuario));
    		user.setIdioma(idioma);
    		
    		String responseJSON = mapper.cambiarPasswordUsuario(user);
    		LOGGER.debug("[RESPUESTA DE BASE DE DATOS: ] " + responseJSON);
    		
    		user = gson.fromJson(responseJSON, TUsuarios.class);
    		response.setIdError(user.getIdError());
    		response.setMensajeError(user.getMensajeError());
    		
    		if(user.getIdError() == 0)
    		{
    			if(idioma.equalsIgnoreCase("es")){    				
    				UtilsMC.emailSend(user.geteMail(),mapper.getParameter("@MENSAJE_RECOVERYPWD_TELECOM_ES"), mapper.getParameter("@ASUNTO_RECOVERYPWD_TELECOM_ES"), user.getUsrLogin(),"",  password, user.getUsrNombre(), new String[]{Constantes.Path_images+"user_update_password.PNG"});
    			}
    			else{    				
    				UtilsMC.emailSend(user.geteMail(),mapper.getParameter("@MENSAJE_RECOVERYPWD_TELECOM_EN"), mapper.getParameter("@ASUNTO_RECOVERYPWD_TELECOM_EN"), user.getUsrLogin(),"", password,user.getUsrNombre(),new String[]{Constantes.Path_images+"user_update_password.PNG"});
    			}
    		}
    		
    	}catch(Exception ex){
    		LOGGER.debug("[ERROR AL ACTUALIZAR PASWORD] ", ex);
    		response.setIdError(101);
    		if(idioma.equalsIgnoreCase("es"))
    			response.setMensajeError("Error inesperado, intente de nuevo");
    		else
    			response.setMensajeError("Unexpected error, try again");
    	}
    	LOGGER.debug("[FINALIZANDO ACTUALIZACION DE PASSWORD ] USER: " + idUsuario );
    	return response;
    }
    
    public BaseResponse passwordReset(String userOrEmail, String idioma){
    	LOGGER.debug("[INICIANDO RESET DE PASSWORD ] login: " + userOrEmail );
    	TUsuarios user = new TUsuarios();
    	BaseResponse response = new BaseResponse();
    	
    	try{
    		
    		String password = UtilsMC.generatePassword(8);
    		LOGGER.debug("password: "+password);
    		String passr = UtilsMC.generaSemillaAux(password);
    		LOGGER.debug("passr: "+passr);
    	    String passrc = Crypto.aesEncrypt(passr, password);
    	    LOGGER.debug("passrc: "+passrc);
    	    user.setUsrPwd(passrc);    	    
    		user.setUsrLogin(userOrEmail);
    		user.setIdioma(idioma);
    		
    		String responseJSON = mapper.resetPasswordUsuario(user);
    		LOGGER.debug("[RESPUESTA BASE DE DATOS] " + responseJSON.replace("email", "eMail").replace("nombres", "usrNombre"));
    		
    		user =  gson.fromJson(responseJSON.replace("email", "eMail").replace("nombres", "usrNombre"), TUsuarios.class);
    		response.setIdError(user.getIdError());
    		response.setMensajeError(user.getMensajeError());
    		
    		if(user.getIdError() == 0){
    			if(idioma.equalsIgnoreCase("es")){    				
    				UtilsMC.emailSend(user.geteMail(),mapper.getParameter("@MENSAJE_RECOVERYPWD_TELECOM_ES"), mapper.getParameter("@ASUNTO_RECOVERYPWD_ES"), userOrEmail, "",password, user.getUsrNombre(), new String[]{Constantes.Path_images+"user_reset_password.PNG"});
    			}else{    				
    				UtilsMC.emailSend(user.geteMail(),mapper.getParameter("@MENSAJE_RECOVERYPWD_TELECOM_EN"), mapper.getParameter("@ASUNTO_RECOVERYPWD_TELECOM_EN"), userOrEmail,"", password, user.getUsrNombre(),new String[]{Constantes.Path_images+"user_reset_password.PNG"});
    			}
    		}
    		
    	}catch(Exception ex){
    		LOGGER.debug("[ERROR AL RESETEAR PASWORD] USER: " + userOrEmail, ex);
    		response.setIdError(101);
    		if(idioma.equalsIgnoreCase("es"))
    			response.setMensajeError("Error inesperado, intente de nuevo");
    		else
    			response.setMensajeError("Unexpected error, try again");
    	}
    	LOGGER.debug("[FINALIZANDO RESET DE PASSWORD ] USER: " + userOrEmail );
    	return response;
    }
    
    public UpdateResponse userUpdateInfo(String idioma, String name, long idUsuario, String value){
    	LOGGER.debug("[INICIANDO PROCESO DE ACTUALIZACION ] [" + name + "] " + idUsuario);
    	UpdateResponse response = new UpdateResponse();
    	try{
    		
    		String responseJSON = mapper.updateUserInfo(idioma,name, value, idUsuario);
    		LOGGER.debug("[RESPUESTA BD] "+ responseJSON);
    		response = gson.fromJson(responseJSON, UpdateResponse.class);
    		
    	}catch(Exception ex){
    		LOGGER.debug("[ERROR AL ACTUALIZAR DATOS] USER: " + idUsuario, ex);
    		getBaseResponse(idioma, response, 101);
    	}
    	LOGGER.debug("[FINALIZANDO PROCESO DE ACTUALIZACION ] [" + name + "] " + idUsuario);
    	return response;
    }
    
    
   public TermResponse getTerms(String idioma, int idpais, String pass ){
    	
    	TermResponse response = new TermResponse();
    	response.setIdError(0);
    	response.setMensajeError("");
    	
    	String hash_android = UtilsMC.getSHA256("mobilecardandroid");
    	String hash_ios = UtilsMC.getSHA256("mobilecardios");
    	
    	LOGGER.debug("android: " + pass  + "\t " + hash_android);
    	LOGGER.debug("ios: " + pass  + "\t " + hash_ios);
    	try{
    		if(pass.equals(hash_ios) || pass.equals(hash_android)){
	    		
    			String term = mapper.getTerminos(idpais, idioma);
	    		response.setTermino(term);
	    		if(term == null || term.isEmpty()){
	    			getBaseResponse(idioma, response,90);
	    		}
    		}
    		else
    		{
    			response.setIdError(91);
    			if(idioma.equalsIgnoreCase("es"))
    				response.setMensajeError("Acceso denegado");
    			else
    				response.setMensajeError("Access denied");
    		}
    	}catch(Exception ex){
    		getBaseResponse(idioma, response,92);
    	}
    	return response;
    }
    
    
    private void getBaseResponse(String idioma, BaseResponse response, int cod){
    	
    	response.setIdError(cod);
		if(idioma.equalsIgnoreCase("es"))
			response.setMensajeError("Error inesperado, vuelva a intentarlo");
		else
			response.setMensajeError("Unexpected error, please try again");
    }
    

}