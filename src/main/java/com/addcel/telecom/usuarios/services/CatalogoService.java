package com.addcel.telecom.usuarios.services;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


import com.addcel.telecom.usuarios.model.vo.AbstractVO;
import com.addcel.telecom.usuarios.utils.Constantes;

import com.google.gson.Gson;

@Service
public class CatalogoService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioService.class);
	
	private static final Gson GSON = new Gson();
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat(Constantes.FORMATO_FECHA_ENCRIPT);
	
	public String consultarCatalogo(String json) {
		
		AbstractVO res = null;
        LOGGER.info("consultarCatalogo");
        com.addcel.telecom.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub binding = null;
         try {
                    binding = (com.addcel.telecom.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub)
                          new com.addcel.telecom.usuarios.ws.clientes.bridge.MCUsuariosBridgeWS_ServiceLocator().getMCUsuariosBridgeWSPort();
                } catch (javax.xml.rpc.ServiceException jre) {
                    if (jre.getLinkedCause() != null) {
                        jre.getLinkedCause().printStackTrace();
                    }
                }
		try {
			 json= binding.consultaCatalogos(json);
             System.out.println("Respuesta consultarCatalogo :  " + json);
		} catch(org.apache.axis.AxisFault axisFaultException){
			System.out.println("Ocurrio un error consultarCatalogo: " + axisFaultException.getMessage());
		} catch(java.rmi.RemoteException re){
			System.out.println("Ocurrio un error RemoteException consultarCatalogo: " + re);
		}
                catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(10);
			res.setMensajeError(Constantes.ERROR_INESPERADO_ALTA_USUARIO);
			json = GSON.toJson(res);
			LOGGER.error(Constantes.LOG_PROCESO_CONSULTA_CATALOGO+"["+json+"]");
			pe.printStackTrace();
		} finally {
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
                
		return json;
	}


}
