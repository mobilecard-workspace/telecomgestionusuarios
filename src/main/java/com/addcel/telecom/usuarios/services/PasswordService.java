package com.addcel.telecom.usuarios.services;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.addcel.telecom.usuarios.model.vo.AbstractVO;
import com.addcel.telecom.usuarios.utils.Constantes;
import com.addcel.telecom.usuarios.ws.clientes.bridge.MCUsuariosBridgeWSPortBindingStub;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
public class PasswordService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PasswordService.class);
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat(Constantes.FORMATO_FECHA_ENCRIPT);
	
	//23 88 -- Clave Youtube Kids
	
	private static Gson GSON = new Gson();

	public String cambioPassword(String json) {
		AbstractVO res = null;
		try {
			json =  AddcelCrypto.decryptSensitive(json);
			LOGGER.info(Constantes.LOG_PROCESO_CAMBIO_PASSWORD+"["+json+"]");
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
			MCUsuariosBridgeWSPortBindingStub bridgeWSProxy = new MCUsuariosBridgeWSPortBindingStub();
			json = bridgeWSProxy.cambiarPassword(json);
			json =  AddcelCrypto.decryptSensitive(json);
                        
		} catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(-1);
			res.setMensajeError(Constantes.ERROR_CAMBIO_PASSWORD);
			json = GSON.toJson(res);
			LOGGER.error(Constantes.LOG_PROCESO_CAMBIO_PASSWORD+"["+json+"]");
			pe.printStackTrace();
		}finally {
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
		return json;
	}
	
	public String resetPassword(String json) {
		AbstractVO res = null;
		try {
			json =  AddcelCrypto.decryptSensitive(json);
			LOGGER.info(Constantes.LOG_PROCESO_RESET_PASSWORD+"["+json+"]");		
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
			MCUsuariosBridgeWSPortBindingStub bridgeWSProxy = new MCUsuariosBridgeWSPortBindingStub();
			json = bridgeWSProxy.resetPassword(json);
			json =  AddcelCrypto.decryptSensitive(json);
		} catch (Exception pe) {
			res = new AbstractVO();
			res.setIdError(-1);
			res.setMensajeError(Constantes.ERROR_RESET_PASSWORD);
			json = GSON.toJson(res);
			LOGGER.error(Constantes.LOG_PROCESO_RESET_PASSWORD+"["+json+"]");
			pe.printStackTrace();
		}finally {
			LOGGER.info(Constantes.LOG_RESPUESTA+"["+json+"]");
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
		}
		return json;
	}
}
